﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breaker : MonoBehaviour {

	float radiusToCheck = 0.15f;
	// Use this for initialization
	void Start () {
		
	}

	private void Update() {
			//print( "active" );
			Collider[] hitColliders = Physics.OverlapSphere( transform.position, radiusToCheck );
			foreach ( Collider col in hitColliders ) {
				if ( col.GetComponent<Breakable>() ) {
					col.GetComponent<Breakable>().BreakFunction();
					break;
				}
			}
		}
}
