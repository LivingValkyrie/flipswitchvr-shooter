﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SceneTransition
/// </summary>
public class SceneTransition : MonoBehaviour {
	#region Fields

	public float fadeSpeed = .5f;
	float alpha = 0;
	public Texture2D fadeOutTexture;
	int drawDepth = -1000;
	int fadeDir = -1;
	public OVRScreenFade fader;
	public Vector3 labOrigin;

	#endregion

	void Start() {
		DontDestroyOnLoad(gameObject);
		//fader.fadeEvent += LoadScene;
		StartCoroutine( PrepareScene( "LVL_lab-demo" ) );
	}

	public void LoadScene(string levelName) {
		fader.LoadLabLevel();
		//StartCoroutine(LoadAfterFade(levelName));
		//SceneManager.LoadSceneAdditive()
	}

	public GameObject loadButton;
	public float scenePrepTime = 3.5f;
	IEnumerator PrepareScene( string levelName ) {
		//yield return new WaitForSeconds( scenePrepTime );
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync( levelName, LoadSceneMode.Additive );
		while ( !asyncLoad.isDone ) {
			yield return null;
		}
		loadButton.SetActive( true );

		print( "level loaded");
	}

	IEnumerator LoadAfterFade( string levelName ) {
		print(" start fade");
		fader.FadeOut();
		
		SceneManager.LoadScene( levelName );
		print( "after scene load" );

		fader.FadeIn();
		yield return null;
	}

	//void OnGUI() {
	//	alpha += fadeDir * fadeSpeed * Time.deltaTime * 2;
	//	alpha = Mathf.Clamp01( alpha );

	//	GUI.color = new Color( GUI.color.r, GUI.color.g, GUI.color.b, alpha );
	//	GUI.depth = drawDepth;
	//	GUI.DrawTexture( new Rect( 0, 0, Screen.width, Screen.height ), fadeOutTexture );
	//}
}