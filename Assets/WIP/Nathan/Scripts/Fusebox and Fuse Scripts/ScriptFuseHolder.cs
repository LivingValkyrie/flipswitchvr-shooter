﻿using UnityEngine;

public enum FuseHolder
{
    FuseHolder1,
    FuseHolder2,
    FuseHolder3
};

public class ScriptFuseHolder : MonoBehaviour {

    public FuseHolder thisFuse;
    public bool isAttached;
	
    public void RpcAttachFuse(bool isPositive)
    {
        FusePanelScript.AddFuse(thisFuse, isPositive);
        isAttached = true;
    }
	
    public void RpcDetachFuse()
    {
        FusePanelScript.RemoveFuse(thisFuse);
        isAttached = false;
    }

}
