﻿
using UnityEngine;

public static class FusePanelScript
{
	//Local declarations
	static bool fuseOne, fuseTwo, fuseThree, firstFuseDone, thirdFuseDone;  //Is there a fuse inserted into slots A, B, and C
	static bool powerOn = false;
	static int numFuses = 0;

	static Animator anim = GameObject.Find("fuse_box").GetComponent<Animator>();
	static Transform fuseLever = GameObject.Find("fuse_box").transform.GetChild(0);

	public static bool CheckFuse(int fuseSlot) {
		switch (fuseSlot) {
			case 1:
				return fuseOne;
			case 2:
				return fuseTwo;
			case 3:
				return fuseThree;
			default:
				return false;
		}
	}

	public static void AddFuse(FuseHolder holder, bool isPositive)    //Allow a specified fuse to be set to + or -
	{
		switch (holder)
		{
			case FuseHolder.FuseHolder1:
				fuseOne = isPositive;
				break;
			case FuseHolder.FuseHolder2:
				fuseTwo = isPositive;
				break;
			case FuseHolder.FuseHolder3:
				fuseThree = isPositive;
				break;
		}
		numFuses++;

	}

	public static void RemoveFuse(FuseHolder holder)
	{
		// If there's three fuses, toggle the power to turn it off
		if (numFuses == 3 && powerOn)
			TogglePower();

		switch (holder)
		{
			case FuseHolder.FuseHolder1:
				fuseOne = false;
				break;
			case FuseHolder.FuseHolder2:
				fuseTwo = false;
				break;
			case FuseHolder.FuseHolder3:
				fuseThree = false;
				break;
		}

		if (numFuses > 0)
			numFuses--;
		
	}

	// Turns on the power if there are three fuses
	// Return to the fuse switch call to tell it if it was successful or not
	public static void TogglePower()
	{

		// Power was on, so do things to turn off
		if (powerOn)
		{
			Debug.Log("Unpulling the lever");
			anim.Play("UnPullLeverAnim");
			fuseLever.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
		}
		// It's off, so if there's three fuses, turn it on and trigger the corresponding event
		else
		{
			if (numFuses == 3)
			{
				Debug.Log("Pulling the lever");
				anim.Play("PullLeverAnim");
				fuseLever.rotation = Quaternion.Euler(new Vector3(180, 0, 0));
			}
		}

		// Toggle it
		powerOn = !powerOn;

		// Check the fuses
		CheckFuses();
	}
	
	public static void CheckFuses()   //Execute the effects according to the current fuse combination on powering the fuse panel on or off
	{
		//These will happen the frame power is turned on with these solutions in place
		if (powerOn)
		{
			Debug.Log("Checking the fuses with powerOn = true");
			if (!fuseOne && !fuseTwo && !fuseThree)
			{
				//Check to see if first fuse has been found
				if (!firstFuseDone)
				{
					//Open door
					ScriptEventManager.CmdTriggerEvent("---");
					firstFuseDone = true;
				}

				Debug.Log("-,-,- : Eric comes in");
			}

			else if (fuseOne && !fuseTwo && !fuseThree)
			{
				//Turn on lights containing the LightController script
				ScriptEventManager.CmdTriggerEvent("+--");
				Debug.Log("+,-,- : lights flickering");
			}

			else if (!fuseOne && !fuseTwo && fuseThree)
			{
				//Turn on jukebox
				ScriptEventManager.CmdTriggerEvent("--+");
				Debug.Log("-,-,+ : Jukebox on");
			}

			else if (!fuseOne && fuseTwo && !fuseThree)
			{
				//Turns on gravity distortion device, revealing second fuse
				//Trigger haptic vest
				ScriptEventManager.CmdTriggerEvent("-+-");
				Debug.Log("-,+,- : Gravity off");
			}

			else if (fuseOne && fuseTwo && !fuseThree)
			{
				//Play animation of generators overloading
				ScriptEventManager.CmdTriggerEvent("++-");
				Debug.Log("+,+,- : Generator");
			}

			else if (!fuseOne && fuseTwo && fuseThree)
			{
				//Turn on monitor, play video
				ScriptEventManager.CmdTriggerEvent("-++");
				Debug.Log("-,+,+ : Monitors");
			}

			else if (fuseOne && !fuseTwo && fuseThree)
			{
				//Activate ventilation system, causing third fuse to fall out
				//Activate pis for small air burst
				if (!thirdFuseDone)
				{
					ScriptEventManager.CmdTriggerEvent("+-+");
					thirdFuseDone = true;
				}

				Debug.Log("+,-,+ : Air Vent");
			}

			else if (fuseOne && fuseTwo && fuseThree)
			{
				//Re-enable lift controls
				ScriptEventManager.CmdTriggerEvent("+++");
				Debug.Log("+,+,+ : Elevator");
			}
		}
		// These will happen the frame power is turned off with these solutions in place
		else
		{
			if (fuseOne && !fuseTwo && !fuseThree)
			{
				//Turn off lights specified in the lights array
				ScriptEventManager.CmdTriggerEvent("+--");
				Debug.Log("+,-,-");
			}

			else if (!fuseOne && !fuseTwo && fuseThree)
			{
				//Turn off jukebox
				ScriptEventManager.CmdTriggerEvent("--+");
				Debug.Log("-,-,+");
			}

			else if (!fuseOne && fuseTwo && !fuseThree)
			{
				//Turns off gravity distortion device
				//Stop triggering haptic vest
				ScriptEventManager.CmdTriggerEvent("-+-");
				Debug.Log("-,+,-");
			}

			else if (fuseOne && fuseTwo && !fuseThree)
			{
				//Turns off gravity distortion device
				//Stop triggering haptic vest
				ScriptEventManager.CmdTriggerEvent("++-");
				Debug.Log("+,+,-");
			}

			else if (!fuseOne && fuseTwo && fuseThree)
			{
				//Turn off monitor
				ScriptEventManager.CmdTriggerEvent("-++");
				Debug.Log("-,+,+");
			}

			else if (fuseOne && fuseTwo && fuseThree)
			{
				//Disable lift controls
				ScriptEventManager.CmdTriggerEvent("+++");
				Debug.Log("+,+,+");
			}
		}
	}
}