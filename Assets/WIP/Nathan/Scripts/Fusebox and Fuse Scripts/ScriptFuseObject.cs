﻿using UnityEngine;

public class ScriptFuseObject : MovableObject {

    [Header("Fuse Options")]
    public bool isPositive;

    public Vector3 attachRot;
    public Vector3 attachOffset;
    private bool isAttached = false;
    ScriptFuseHolder attachedHolder;

    protected override void Start()
    {
        GetComponent<Renderer>().material.color =  isPositive ? Color.green : Color.red;
		base.Start();
    }
    public override void OnActivate()
    {
        base.OnActivate();
        GetComponent<Rigidbody>().isKinematic = true;
        transform.rotation = Quaternion.Euler(attachRot);
        if (isAttached)
        {
            attachedHolder.RpcDetachFuse();
            attachedHolder = null;
            isAttached = false;
        }     
    }

    public override void OnDeactivate()
    {
        base.OnDeactivate();
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radiusToCheck);
        foreach(Collider col in hitColliders)
        {
            if (col.gameObject.tag == "FuseHolder" && !col.GetComponent<ScriptFuseHolder>().isAttached)
            {
                transform.position = col.transform.position + attachOffset;
                transform.rotation = Quaternion.Euler(attachRot);
                attachedHolder = col.gameObject.GetComponent<ScriptFuseHolder>();
                attachedHolder.RpcAttachFuse(isPositive);
                isAttached = true;
                break;
            }
        }

        GetComponent<Rigidbody>().isKinematic = isAttached;
    }



	private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radiusToCheck);
    }
}
