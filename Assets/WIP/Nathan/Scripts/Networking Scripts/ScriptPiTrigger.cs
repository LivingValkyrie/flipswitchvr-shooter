﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

/*
 * Author: Nathan Boehning
 */

public enum PhysicalEffect
{
	Lift,
	MakeNoise,
	Decon,
	Flash
}

public struct message
{
	public int msgCode;
	public string piName;

	public message(int code, string name)
	{
		msgCode = code;
		piName = name;
	}

}

public class MessageCollection
{
	public static message Lift = new message(1001, "pi1002");
	public static message MakeNoise = new message(1001, "pi1003");
	public static message Decon = new message(1001, "pi1004");
	public static message Flash = new message(1001, "pi1005");
}

public static class ScriptPiTrigger
{
	public static void TriggerPi(PhysicalEffect effect)
	{
		switch (effect)
		{
			case PhysicalEffect.Lift:
				Debug.Log("asedfesdfasef");
                    ScriptClientSocket.sendMessage(MessageCollection.Lift);
				    break;
			case PhysicalEffect.Decon:
				Debug.Log( "lklhkhlk" );

				ScriptClientSocket.sendMessage(MessageCollection.Decon);
				    break;
			case PhysicalEffect.Flash:
				    ScriptClientSocket.sendMessage(MessageCollection.Flash);
				    break;
			case PhysicalEffect.MakeNoise:
				    ScriptClientSocket.sendMessage(MessageCollection.MakeNoise);
				    break;
		}
	}
}
