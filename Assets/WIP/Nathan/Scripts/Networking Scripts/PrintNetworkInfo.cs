﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class PrintNetworkInfo : NetworkBehaviour {

	// Use this for initialization
	void Start () {
		Debug.Log(gameObject.name + " " + GetComponent<NetworkIdentity>().netId);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
