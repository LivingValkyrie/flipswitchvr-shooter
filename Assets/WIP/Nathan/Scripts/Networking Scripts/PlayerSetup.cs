﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class PlayerSetup : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponentInChildren<OptitrackStreamingClient>().LocalAddress = GetLocalIPAddress();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new Exception("Local IP Address not Found!");
    }
}
