﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ScriptSyncPlayer : NetworkBehaviour {

    [SyncVar]
    public Color playerColor;

    [SyncVar]
    public string playerName;

	private void Start()
    {
        if(isLocalPlayer)
        {
            CmdSendName(gameObject, "Player " + Network.connections.Length + 1);
            transform.name = playerName;
        }

        SetupPlayerName();
    }

    [Command]
    private void CmdSendName(GameObject player, string name)
    {
        player.GetComponent<ScriptSyncPlayer>().playerName = name;
    }

    private void SetupPlayerName()
    {
        if(!isLocalPlayer && playerName != "")
        {
            transform.name = playerName;
        }
    }
}
