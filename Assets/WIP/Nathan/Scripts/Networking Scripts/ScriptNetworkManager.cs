﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Networking;


public class ScriptNetworkManager : NetworkManager {

    public GameObject hostPrefab;
    public string hostMachineAddress;
    private static bool firstTime = true;
    public bool isTesting;


    void Start()
    {
        if (!isTesting)
        {
            if (hostMachineAddress.Equals(GetLocalIPAddress()))
            {
                Debug.Log("is host machine");
                singleton.networkAddress = hostMachineAddress;
                singleton.networkPort = 7777;
                singleton.StartHost();
            }
            else if (hostMachineAddress != GetLocalIPAddress())
            {
                Debug.Log("Is client machine");
                singleton.networkAddress = hostMachineAddress;
                singleton.networkPort = 7777;
                singleton.StartClient();
            }
        }
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        if (!isTesting)
        {
            if (firstTime)
            {
                Debug.Log("Host is connecting");
                GameObject host = Instantiate(hostPrefab, GetStartPosition().transform.position, Quaternion.identity);
                NetworkServer.AddPlayerForConnection(conn, host, playerControllerId);
                firstTime = false;
            }
            else
            {
                Debug.Log("Client is connecting");
                GameObject player = Instantiate(playerPrefab, GetStartPosition().transform.position, Quaternion.identity);
                NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
            }
        }
        else
        {
            GameObject player = Instantiate(playerPrefab, GetStartPosition().transform.position, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        }
    }


    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new Exception("Local IP Address not Found!");
    }
}
