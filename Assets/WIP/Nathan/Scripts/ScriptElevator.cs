﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class ScriptElevator : MonoBehaviour {

	public float timeToLevelTwo;
	public float timeToLevelThree;
	
	public bool levelComplete = false;
	public float[] levelYPos;
	
	public int curLevel = 1;
    public int totalPlayers = 1;
	
	private ScriptNetworkManager netManScript;
	//private bool isMoving = false;
	private int feetOnLift = 0;

    UnityAction fuseboxListener;
    private bool fusePuzzleComplete;

    void Awake()
    {
       // fuseboxListener = new UnityAction(LevelTwoComplete);
    }

    //void OnEnable()
    //{
    //    ScriptEventManager.StartListening("+++", fuseboxListener);
    //}

    //void OnDisable()
    //{
    //    ScriptEventManager.StopListening("+++", fuseboxListener);
    //}

   // Use this for initialization

   //void Start()
   // {

   //    netManScript = GameObject.Find("NetworkManager").GetComponent<ScriptNetworkManager>();
   // }

    // Update is called once per frame
    //public void StartElevator ()
    //{
    //	switch (curLevel)
    //	{
    //		case 1:
    //			//Debug.Log("Players on lift: " + playersOnLift);
    //			//Debug.Log("Players in game: " + netManScript.numPlayers);
    //			if(!isMoving && levelComplete)
    //			{
    //				Debug.Log("Level one is complete, elevator should start moving");
    //				Vector3 targetPos = new Vector3(transform.position.x, levelYPos[0], transform.position.z);
    //				Vector3 startPos = transform.position;
    //				StartCoroutine(MoveElevator(startPos, targetPos, timeToLevelTwo));
    //				levelComplete = false;
    //				curLevel++;
    //			}

    //			break;
    //		case 2:
    //			if(!isMoving && levelComplete)
    //			{
    //				Debug.Log("Level two is complete, elevator should start moving");
    //				Vector3 targetPos = new Vector3(transform.position.x, levelYPos[1], transform.position.z);
    //				Vector3 startPos = transform.position;
    //				StartCoroutine(MoveElevator(startPos, targetPos, timeToLevelThree));
    //				levelComplete = false;
    //				curLevel++;
    //			}
    //			break;
    //	}
    //}

    //IEnumerator MoveElevator(Vector3 start, Vector3 end, float overTime)
    //{
    //	Debug.Log("Elevator should be moving");
    //	float startTime = Time.time;
    //       Transform playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

    //       Vector3 playerStart = playerTransform.position;
    //       Vector3 playerEnd = new Vector3(playerTransform.position.x, levelYPos[0]+1.5f, playerTransform.position.z);
    //       isMoving = true;
    //	while (Time.time < startTime + overTime)
    //	{
    //		transform.position = Vector3.Lerp(start, end, (Time.time - startTime) / overTime);
    //           playerTransform.position = Vector3.Lerp(playerStart, playerEnd, (Time.time - startTime) / overTime);
    //		yield return null;
    //	}
    //	transform.position = end;
    //}

    void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Foot")
        {
            feetOnLift++;
            Debug.Log("Foot entered lift");
        }

        CheckIfComplete();

    }
	
	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Foot")
        {
            Debug.Log("Foot exited lift");
            feetOnLift--;
        }

        levelComplete = false;
	}

    public void fuseComplete()
    {
        fusePuzzleComplete = true;
    }

    private void CheckIfComplete()
    {
        if (feetOnLift == 2 && fusePuzzleComplete)
            levelComplete = true;
    }
}
