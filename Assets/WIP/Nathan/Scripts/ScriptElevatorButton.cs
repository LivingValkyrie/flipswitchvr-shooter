﻿using UnityEngine;

public class ScriptElevatorButton : FuseEventSwitch {

    private ScriptElevator eleScript;


    private void Start()
    {
        eleScript = GetComponentInParent<ScriptElevator>();
    }
    public override void Toggle(GameObject activatingObject)
    {
        if (oneShot)
        {
            if (!triggered)
            {
                if (eleScript.levelComplete)
                {
                    base.Toggle(activatingObject);
                    ScriptPiTrigger.TriggerPi(PhysicalEffect.Lift);
                    triggered = true;
                }
            }
        }
        else
        {
            if (FusePanelScript.CheckFuse(fuseSlotNumber))
            {
                base.Toggle(activatingObject);
            }
        }
    }

    public override void OnActivate(){}

    public override void OnDeactivate(){}
}
