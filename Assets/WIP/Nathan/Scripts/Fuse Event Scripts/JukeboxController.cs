﻿using UnityEngine.Events;
using UnityEngine;

public class JukeboxController : MonoBehaviour
{
    private UnityAction fuseboxListener;
    bool isOn;

    void Awake()
    {
        fuseboxListener = new UnityAction(Toggle);
    }

    void OnEnable()
    {
        ScriptEventManager.StartListening("--+", fuseboxListener);
    }
    
    void OnDisable()
    {
        ScriptEventManager.StopListening("--+", fuseboxListener);
    }

    //Play and stop jukebox music
    private void Toggle()
    {
        isOn = !isOn;
        if (isOn)
            GetComponent<AudioSource>().Play();
        else
            GetComponent<AudioSource>().Stop();
    }
}
