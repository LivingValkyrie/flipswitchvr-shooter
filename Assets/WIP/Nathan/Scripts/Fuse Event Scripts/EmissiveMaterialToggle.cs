﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;


public enum EmissiveEventObjects
{
    Generator,
    Jukebox,
    Monitor
};

public class EmissiveMaterialToggle : MonoBehaviour
{
    public EmissiveEventObjects type;
    private UnityAction fuseboxListener;
    bool isOn = false;
    public Material[] emissiveMaterials;
    public AudioClip powerOnSound;

    private AudioSource src;

    void Awake()
    {
        fuseboxListener = new UnityAction(TogglePower);
    }

    private void Start()
    {
        src = GetComponent<AudioSource>();
        for (int i = 0; i < emissiveMaterials.Length; i++)
            emissiveMaterials[i].DisableKeyword("_EMISSION");
    }

    void OnEnable()
    {
        switch (type)
        {
            case EmissiveEventObjects.Generator:
                ScriptEventManager.StartListening("++-", fuseboxListener);
                break;
            case EmissiveEventObjects.Jukebox:
                ScriptEventManager.StartListening("--+", fuseboxListener);
                break;
            case EmissiveEventObjects.Monitor:
                ScriptEventManager.StartListening("-++", fuseboxListener);
                break;
        }
    }

    void OnDisable()
    {
        switch (type)
        {
            case EmissiveEventObjects.Generator:
                ScriptEventManager.StopListening("++-", fuseboxListener);
                break;
            case EmissiveEventObjects.Jukebox:
                ScriptEventManager.StopListening("--+", fuseboxListener);
                break;
            case EmissiveEventObjects.Monitor:
                ScriptEventManager.StopListening("-++", fuseboxListener);
                break;
        }
    }

    void TogglePower()
    {
        if (!isOn)
        {
            isOn = true;
            src.clip = powerOnSound;
            src.Play();
            for (int i = 0; i < emissiveMaterials.Length; i++)
                emissiveMaterials[i].EnableKeyword("_EMISSION");
        }
        else
        {
            isOn = false;
            src.Stop();
            for (int i = 0; i < emissiveMaterials.Length; i++)
                emissiveMaterials[i].DisableKeyword("_EMISSION");
        }
    }
}
