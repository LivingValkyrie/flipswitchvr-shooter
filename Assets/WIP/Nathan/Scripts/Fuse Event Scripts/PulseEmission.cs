﻿using UnityEngine;
using UnityEngine.Events;

public class PulseEmission : MonoBehaviour
{
    public Color BaseColor = Color.red;
    private UnityAction fuseboxListener;
    bool isOn = false;
    private Material _material;

    void Update()
    {
        if(isOn)
            _material.SetColor("_EmissionColor", BaseColor * Mathf.LinearToGammaSpace(Mathf.PingPong(Time.time, 0.5f)));
    }


    void Awake()
    {
        fuseboxListener = new UnityAction(Toggle);
        _material = GetComponent<Renderer>().material;
    }

    void OnEnable()
    {
        ScriptEventManager.StartListening("-+-", fuseboxListener);
    }

    void OnDisable()
    {
        ScriptEventManager.StopListening("-+-", fuseboxListener);
    }

    void Start()
    {
        _material.DisableKeyword("_EMISSION");
        //Toggle();
    }

    void Toggle()
    {
        isOn = !isOn;
        if (isOn)
        {
            _material.EnableKeyword("_EMISSION");
            GetComponent<AudioSource>().Play();
        }
        else
        {
            _material.DisableKeyword("_EMISSION");
            GetComponent<AudioSource>().Stop();
        }
    }
}
