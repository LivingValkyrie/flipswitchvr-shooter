﻿using UnityEngine;
using UnityEngine.Events;

public class ScriptAirVent : MonoBehaviour {

    private UnityAction fuseboxListener;
    private Animator anim;

    void Awake()
    {
        fuseboxListener = new UnityAction(OpenVent);
    }

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void OnEnable()
    {
        ScriptEventManager.StartListening("+-+", fuseboxListener);
    }

    void OnDisable()
    {
        ScriptEventManager.StopListening("+-+", fuseboxListener);
    }

    void OpenVent()
    {
        anim.Play("OpenAirVent");
        GetComponent<AudioSource>().Play();
    }
}
