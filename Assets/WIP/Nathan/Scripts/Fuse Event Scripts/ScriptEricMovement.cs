﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;
using System.Collections;

public class ScriptEricMovement : MonoBehaviour
{
    // Variables for nav mesh movement

	[SerializeField]
    [Tooltip("--- : Go into room")]
	Transform firstPermutationPosition;

    [SerializeField]
    [Tooltip("+-- : Move into lit area")]
    Transform secondPermutationPosition;

    [SerializeField]
    [Tooltip("--+ : Jukebox")]
    Transform thirdPermutationPosition;

    [SerializeField]
    [Tooltip("++- : Move to generator")]
    Transform fifthPermutationPosition;

    [SerializeField]
    [Tooltip("-++ : Look at monitor playing video")]
    Transform sixthPermutationPosition;

    [SerializeField]
    [Tooltip("+-+ : Go to third [+] piece")]
    Transform seventhPermutationPosition;
    
    [SerializeField]
    [Tooltip("+++ : Get on elevator?")]
    Transform eigthPermutationPosition;

    private NavMeshAgent agent;
    public ThirdPersonCharacter character { get; private set; } // the character we are controlling
    private bool firstEvent;
    // Variables for unity events
    private UnityAction [] fuseboxListeners = new UnityAction[8];


    // Use awake to set what the listener is
    private void Awake()
    {
        // Listener for fusebox Permutation: ---
        fuseboxListeners[0] = new UnityAction(RpcFirstPermutation);

        // Listener for fusebox Permutation: +--
        fuseboxListeners[1] = new UnityAction(RpcSecondPermutation);

        // Listener for fusebox Permutation: --+
        fuseboxListeners[2] = new UnityAction(RpcThirdPermutation);

        // Listener for fusebox Permutation: -+-
        fuseboxListeners[3] = new UnityAction(RpcFourthPermutation);

        // Listener for fusebox Permutation: ++-
        fuseboxListeners[4] = new UnityAction(RpcFifthPermutation);

        // Listener for fusebox Permutation: -++
        fuseboxListeners[5] = new UnityAction(RpcSixthPermutation);

        // Listener for fusebox Permutation: +-+
        fuseboxListeners[6] = new UnityAction(RpcSeventhPermutation);

        // Listener for fusebox Permutation: +++
        fuseboxListeners[7] = new UnityAction(RpcEigthPermutation);

    }

    // Use OnEnable to register this listener to the event manager
    private void OnEnable()
    {
        ScriptEventManager.StartListening("---", fuseboxListeners[0]);
        ScriptEventManager.StartListening("+--", fuseboxListeners[1]);
        ScriptEventManager.StartListening("--+", fuseboxListeners[2]);
        ScriptEventManager.StartListening("-+-", fuseboxListeners[3]);
        ScriptEventManager.StartListening("++-", fuseboxListeners[4]);
        ScriptEventManager.StartListening("-++", fuseboxListeners[5]);
        ScriptEventManager.StartListening("+-+", fuseboxListeners[6]);
        ScriptEventManager.StartListening("+++", fuseboxListeners[7]);

    }

    // Use OnDisable to unregister this listener from the event manager 
    // (causes memory leaks if you don't clean up)
    private void OnDisable()
    {
        ScriptEventManager.StopListening("---", fuseboxListeners[0]);
        ScriptEventManager.StopListening("+--", fuseboxListeners[1]);
        ScriptEventManager.StopListening("--+", fuseboxListeners[2]);
        ScriptEventManager.StopListening("-+-", fuseboxListeners[3]);
        ScriptEventManager.StopListening("++-", fuseboxListeners[4]);
        ScriptEventManager.StopListening("-++", fuseboxListeners[5]);
        ScriptEventManager.StopListening("+-+", fuseboxListeners[6]);
        ScriptEventManager.StopListening("+++", fuseboxListeners[7]);
    }

    private void Start()
    {
        // get the components on the object we need ( should not be null due to require component so no need to check )
        agent = GetComponent<NavMeshAgent>();
        character = GetComponent<ThirdPersonCharacter>();

        agent.updateRotation = false;
        agent.updatePosition = true;
    }


    private void Update()
    {
        if (agent.remainingDistance > agent.stoppingDistance)
            character.Move(agent.desiredVelocity, false, false);
        else
            character.Move(Vector3.zero, false, false);

        if(firstEvent)
            if (!agent.pathPending)
                if (agent.remainingDistance <= agent.stoppingDistance)
                    if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                    {
                        // Done
                        Debug.Log("gets to end of path");
                        transform.GetChild(3).SetParent(null);
                        firstEvent = false;
                    }
    }

    // Function for fusebox Permutation: ---
    // Lets Eric into the room
   // [ClientRpc]
    private void RpcFirstPermutation()
    {
        Debug.Log(gameObject.name + ": is performing the --- permutation");
        agent.SetDestination(firstPermutationPosition.position);

        firstEvent = true;
        
        //Unsubscribe from first event, eric can't re enter the room again
        ScriptEventManager.StopListening("---", fuseboxListeners[0]);
    }

    // Function for fusebox Permutation: +--
    // Reroutes power to the lights (Move Eric into a lit area?)
   // [ClientRpc]
    private void RpcSecondPermutation()
    {
        Debug.Log(gameObject.name + ": is performing the +-- permutation");
        agent.SetDestination(secondPermutationPosition.position);
    }

    // Function for fusebox Permutation: --+
    // Reroutes power to juke box, jump eric into the room with jukebox, have him look at it
   // [ClientRpc]
    private void RpcThirdPermutation()
    {
        Debug.Log(gameObject.name + ": is performing the --+ permutation");
        agent.SetDestination(thirdPermutationPosition.position);
    }

    // Function for fusebox Permutation: -+-
    // Gravity goes off, have him float?
   // [ClientRpc]
    private void RpcFourthPermutation()
    {
        Debug.Log(gameObject.name + ": is performing the -+- permutation");
    }

    // Function for fusebox Permutation: ++-
    // Generators explode, run over to gen and run around/next to them, back and forth
   // [ClientRpc]
    private void RpcFifthPermutation()
    {
        Debug.Log(gameObject.name + ": is performing the ++- permutation");
        agent.SetDestination(fifthPermutationPosition.position);
    }

    // Function for fusebox Permutation: -++
    // Video plays, have him go to the monitor and look at the video
  //  [ClientRpc]
    private void RpcSixthPermutation()
    {
        Debug.Log(gameObject.name + ": is performing the -++ permutation");
        agent.SetDestination(sixthPermutationPosition.position);
    }

    // Function for fusebox Permutation: +-+
    // Air vents, third piece falls out, go over and look at it
    //[ClientRpc]
    private void RpcSeventhPermutation()
    {
        Debug.Log(gameObject.name + ": is performing the +-+ permutation");
        agent.SetDestination(seventhPermutationPosition.position);
    }

    // Function for fusebox Permutation: +++
    // Elevator working, go on elevator with players?
   // [ClientRpc]
    private void RpcEigthPermutation()
    {
        Debug.Log(gameObject.name + ": is performing the +++ permutation");
        agent.SetDestination(eigthPermutationPosition.position);
    }
}
