﻿using UnityEngine;
using UnityEngine.Events;

public class AntiGravScript : MonoBehaviour
{
    //Declarations
    public GameObject[] affectedObjects;    //List of objects affected by the device
    public float vertForce; //Amount of force to apply to object to make it float upwards
    public float range; //Effective range of the antigrav device
    private UnityAction fuseboxListener;
    bool isOn;

    void Awake()
    {
        fuseboxListener = new UnityAction(ToggleAntiGrav);
    }

    void OnEnable()
    {
        ScriptEventManager.StartListening("-+-", fuseboxListener);
    }

    void OnDisable()
    {
        ScriptEventManager.StopListening("-+-", fuseboxListener);
    }

    void Start()
    {
        //Initialize variables
        isOn = false;
    }

    void Update()
    {
        //Check to see if floating objects are outside the operational range of the antigrav device, and if they are, cancel the effect on them
        if (isOn)
        {
            for (int i = 0; i < affectedObjects.Length; i++)
            {
                if (Vector3.Distance(affectedObjects[i].transform.position, transform.position) > range)
                {
                    affectedObjects[i].GetComponent<Rigidbody>().useGravity = true;
                }
            }
        }
    }

    //Enables and disables the antigrav device
    private void ToggleAntiGrav()
    {
        if (!isOn)
        {
            isOn = true;
            Debug.Log("AntiGrav should be on");
            for (int i = 0; i < affectedObjects.Length; i++)
            {
                affectedObjects[i].GetComponent<Rigidbody>().useGravity = false;
                affectedObjects[i].GetComponent<Rigidbody>().AddForce(Vector3.up * vertForce);
            }
        }

        else
        {
            isOn = false;

            for (int i = 0; i < affectedObjects.Length; i++)
            {
                affectedObjects[i].GetComponent<Rigidbody>().useGravity = true;
            }
        }
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawWireSphere(transform.position, range);
    //}
}