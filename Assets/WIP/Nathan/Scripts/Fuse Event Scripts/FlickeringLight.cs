﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class FlickeringLight : MonoBehaviour
{

    Light testLight;
    public float minWaitTime;
    public float maxWaitTime;

    private UnityAction fuseboxListener;
    bool isOn = false;


    void Awake()
    {
        fuseboxListener = new UnityAction(FlickerLight);
    }

    void OnEnable()
    {
        ScriptEventManager.StartListening("+--", fuseboxListener);
    }

    void OnDisable()
    {
        ScriptEventManager.StopListening("+--", fuseboxListener);
    }

    void Start()
    {
        testLight = GetComponent<Light>();
        testLight.enabled = false;
    }

    void FlickerLight()
    {
        isOn = !isOn;
        if (isOn)
            StartCoroutine(Flashing());
        else
            StopCoroutine(Flashing());
    }

    IEnumerator Flashing()
    {
        while (isOn)
        {
            yield return new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
            testLight.enabled = !testLight.enabled;
        }
        testLight.enabled = false;
    }
}