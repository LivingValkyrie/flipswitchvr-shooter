﻿using UnityEngine;
using UnityEngine.Events;

public class OpenDoors : MonoBehaviour
{
    private UnityAction fuseboxListener;
    private Animator anim;
    void Awake()
    {
        fuseboxListener = new UnityAction(OpenDoor);
    }

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void OnEnable()
    {
        ScriptEventManager.StartListening("---", fuseboxListener);
    }

    void OnDisable()
    {
        ScriptEventManager.StopListening("---", fuseboxListener);
    }

    void OpenDoor()
    {
        GetComponent<AudioSource>().Play();
        anim.Play("OpenDoors");
    }
}
