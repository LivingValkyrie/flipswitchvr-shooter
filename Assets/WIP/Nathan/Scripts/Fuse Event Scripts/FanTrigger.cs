﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanTrigger : FuseEventSwitch
{
    //private bool triggered = false;
    public override void Toggle(GameObject activatingObject)
    {
        if (oneShot)
        {
            if (!triggered)
            {
                if (FusePanelScript.CheckFuse(fuseSlotNumber))
                {
                    base.Toggle(activatingObject);
                    triggered = true;
                    ScriptPiTrigger.TriggerPi(PhysicalEffect.Decon);
                }
            }
        }
        else
        {
            if (FusePanelScript.CheckFuse(fuseSlotNumber))
            {
                base.Toggle(activatingObject);
            }
        }
    }
}
