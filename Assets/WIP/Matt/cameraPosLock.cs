﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class cameraPosLock : MonoBehaviour
{
    public GameObject posPoint;

	void OnEnable() {
#if UNITY_2017_1_OR_NEWER
		Application.onBeforeRender += OnBeforeRender;
#endif
	}


	void OnDisable() {
#if UNITY_2017_1_OR_NEWER
		Application.onBeforeRender -= OnBeforeRender;
#endif
	}

	void OnBeforeRender() {
		transform.position = posPoint.transform.position;
		transform.GetChild( 0 ).localPosition = Vector3.zero;
		//transform.GetChild( 0 ).localRotation = Quaternion.identity;

		//print( transform.GetChild( 0 ).name );
	}
}
