﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlayerScaler
/// </summary>
public class PlayerScaler : MonoBehaviour {
	#region Fields

	public float heightInInches;


	#endregion
	
	void Start() {
		transform.localScale = (heightInInches * 0.0254f) * Vector3.one;

	}

	void Update() {
		
	}
}