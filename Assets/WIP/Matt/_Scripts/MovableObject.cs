﻿using RootMotion.Demos;
using UnityEngine;

public class MovableObject : SimpleSwitch
{
	//GameObject multitool; //doesnt need stored, already have activator to store it in
	public override void Toggle(GameObject activatingObject) {
		if (activatingObject != null && activatingObject.GetComponent<MultitoolController>()) {
			//print( "passed" );
			isActive = !isActive;
			activator = activatingObject;

			if (isActive) // you're never setting this to true
			{
				//multitool = activatingObject; //not needed
				OnActivate();
			} else {
				OnDeactivate();
			}
		}
	}

	Material original;
	Renderer ren;
	Vector3 origin;
	Quaternion originRot;
	public float maxDistance = 25;

	protected virtual void Start() {
		ren = GetComponent<Renderer>();
		if (ren) {
		original = ren.material;
		}

		origin = transform.position;
		originRot = transform.rotation;
		oldVel = newVel = Vector3.zero;
		lastPos = transform.position;
	}

	public float radiusToCheck = 0.15f;
	Vector3 oldVel, newVel, lastPos;

	private void Update()
	{
		if (isActive)
		{
			//print( "active" );
			Collider[] hitColliders = Physics.OverlapSphere(transform.position, radiusToCheck);
			foreach (Collider col in hitColliders)
			{
				if (col.GetComponent<Breakable>())
				{
					col.GetComponent<Breakable>().BreakFunction();
					break;
				}
			}

			if ( OVRInput.GetDown( OVRInput.Button.PrimaryThumbstick, activator.GetComponent<MultitoolController>().controller ) ) {
				isActive = false;
				Vector3 force = activator.transform.forward * activator.GetComponent<MultitoolController>().forceSize;
				OnDeactivate();
				GetComponent<Rigidbody>().AddForce( force, ForceMode.Impulse );

			}

			oldVel = newVel;
			newVel =   transform.position - lastPos;

		}

		if ((transform.position - origin).magnitude >= maxDistance) {
			GetComponent<Rigidbody>().velocity = Vector3.zero;
			transform.rotation = originRot;
			transform.position = origin;
		}
	}

	public override void OnActivate()
	{
		activator.GetComponent<MultitoolController>().SetHeldObject(transform.gameObject);
		GetComponent<Rigidbody>().isKinematic = true;
		GetComponent<Rigidbody>().Sleep();

		if (ren) {
			ren.material = activator.GetComponent<MultitoolController>().holdingMat;
			ren.material.SetTexture("_MainTex", original.GetTexture("_MainTex"));
		}

		//Destroy(GetComponent<Rigidbody>());
	}


	public override void OnDeactivate()
	{
		activator.GetComponent<MultitoolController>().DropObject();
		GetComponent<Rigidbody>().isKinematic = false;
		GetComponent<Rigidbody>().WakeUp();
		//GetComponent<Rigidbody>().AddForce(newVel, ForceMode.VelocityChange);
		print(newVel);
		if (ren) {
		ren.material = original;
		}
	}

}
