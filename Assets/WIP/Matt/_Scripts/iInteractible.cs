﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;

///// <summary>
///// Author: Matt Gipson
///// Contact: Deadwynn@gmail.com
///// Domain: www.livingvalkyrie.net
///// 
///// Description: iInteractible
///// </summary>
//public interface iInteractible {
//	#region Fields

//	[Header( "Interactivity" )]
//	[Tooltip( "Wether or not the switch is active." )]
//	public bool isActive;

//	//todo these are just here in case we decide to use them later on
//	//public UnityEvent activateEvent;
//	//public UnityEvent deactivateEvent;

//	//this is set by toggle by default.
//	protected GameObject activator;

//	#endregion

//	/// <summary>
//	/// Toggles this Switch and calls the appropriate method. 
//	/// </summary>
//	/// <param name="activatingObject">The object activating this switch. if overriding toggle, and the activator please set this within your method.</param>
//	public virtual void Toggle( GameObject activatingObject = null ) {
//		this.activator = activatingObject;
//		isActive = !isActive;

//		if ( isActive ) {
//			OnActivate();
//		} else {
//			OnDeactivate();
//		}
//	}

//	/// <summary>
//	/// Called when [isActive].
//	/// </summary>
//	public abstract void OnActivate();

//	/// <summary>
//	/// Called when ![isActive].
//	/// </summary>
//	public abstract void OnDeactivate();
//}
//}
