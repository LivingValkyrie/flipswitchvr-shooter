﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: HandController
/// </summary>
public class HandController : Controller {
	#region Fields

	public GameObject indexOpen, indexNear, indexClosed;
	HandState state;
	HandState State {
		get { return state; }
		set {
			state = value;

			////todo update to handle all interact event on frame without storing collider
			//if (value == HandState.Fist) {
			//	if (collide) {
			//		if (other.GetComponent<SimpleSwitch>() is Grabbable) {
			//			print("fist");
			//			other.GetComponent<SimpleSwitch>().Toggle(gameObject);
			//		}
			//	}
			//} else if (value == HandState.Pointing) {
			//	if (collide) {
			//		if (other.GetComponent<SimpleSwitch>() is Grabbable) {
			//			print("point");
			//			other.GetComponent<SimpleSwitch>().Toggle(gameObject);
			//		}
			//	}
			//}
		}
	}

	Dictionary<OVRInput.Button, bool> downState;

	#endregion

	internal override void Start() {
		base.Start();

		downState = new Dictionary<OVRInput.Button, bool>();
	}

	internal override void Update() {
		base.Update();
		UpdateFingers();
	}

	internal override void Interact() {
		Get(OVRInput.Button.PrimaryIndexTrigger);
	}

	void Get(OVRInput.Button button) {
		if (OVRInput.Get(button, controller)) {
			//check if in dictionary
			if (downState.ContainsKey(button)) {
				if (downState.ContainsKey(button)) {
					if (downState[button] == false) {
						GetDown(button);
					}
					downState[button] = true;

					//todo any "get" logic here
					print("get");
				}
			} else {
				//first add means true, as its added in the first get check
				downState.Add(button, true);
				GetDown(button);
			}
		} else {
			GetUp(button);
		}
	}

	void GetDown(OVRInput.Button button) {
		//grabbing
		if (button == OVRInput.Button.PrimaryIndexTrigger) {
			print("Check if grabable");

			Collider[] overlaps = Physics.OverlapSphere(transform.position, 1);

			foreach (Collider other in overlaps) {
				if (other.GetComponent<SimpleSwitch>() is Grabbable) {
					other.GetComponent<SimpleSwitch>().Toggle(gameObject);
				}

				break;
			}
		}
	}

	void GetUp(OVRInput.Button button) {
		//letting go
		if (button == OVRInput.Button.PrimaryIndexTrigger) {
			if (downState.ContainsKey(button)) {
				if (downState[button]) {
					//todo any "get up" logic here
					print("let go");
				}
				downState[button] = false;
			}
		}
	}

	void UpdateFingers() {
		//note: primary on ovr is left hand
		//also, there are 3 state of touch, none, near and touch. then press for buttons
		//if you tet only on controller then primary works a this controller, and secondary on other

		if (OVRInput.Get(OVRInput.Touch.PrimaryIndexTrigger, controller)) {
			indexOpen.SetActive(false);
			indexClosed.SetActive(true);
			indexNear.SetActive(false);
			State = HandState.Closed;
		} else if (OVRInput.Get(OVRInput.NearTouch.PrimaryIndexTrigger, controller)) {
			indexOpen.SetActive(false);
			indexClosed.SetActive(false);
			indexNear.SetActive(true);
			State = HandState.Fist;
		} else {
			State = HandState.Pointing;
			indexOpen.SetActive(true);
			indexClosed.SetActive(false);
			indexNear.SetActive(false);
		}
	}

	//bool collide = false;
	//Collider other;

	//void OnTriggerEnter(Collider other) {
	//	collide = true;
	//	this.other = other;

	//	if (State == HandState.Pointing) {
	//		if (other.GetComponent<SimpleSwitch>()) {
	//			if (other.GetComponent<SimpleSwitch>() is Pressable) {
	//				other.GetComponent<SimpleSwitch>().Toggle(gameObject);
	//			}
	//		}
	//	}
	//}

	//void OnTriggerExit(Collider other) {
	//	collide = false;
	//	this.other = other;
	//}

	public enum HandState {
		Pointing,
		Fist,
		Closed
	}
}