﻿using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Grabbable
/// </summary>
public class Grabbable : SimpleSwitch {
	#region Fields

	#endregion
 
	public override void Toggle(GameObject activatingObject) {
		//base.Toggle(activatingObject);
		activator = activatingObject;
		isActive = !isActive;

		if (activator.GetComponent<Controller>() is HandController) {
			if (isActive) {

				OnActivate();
			} else {
				OnDeactivate();
			}
		}
	}
	public override void OnActivate() {
		GetComponent<Rigidbody>().isKinematic = true;

		transform.SetParent(activator.transform);
	}
	public override void OnDeactivate() {
		GetComponent<Rigidbody>().isKinematic = false;

		transform.SetParent(null);
	}
}