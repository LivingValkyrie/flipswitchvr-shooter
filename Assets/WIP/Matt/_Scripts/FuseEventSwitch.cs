﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: FuseEventSwitch
/// </summary>
public class FuseEventSwitch : EventSwitch {
	#region Fields

	public int fuseSlotNumber;
	public bool oneShot = false;
	protected bool triggered = false;
	public GameObject sparks;

	#endregion

	private void Update() {
		if ( isActive ) {
			GetComponent<Renderer>().material.color = Color.green;
		} else if ( FusePanelScript.CheckFuse( fuseSlotNumber ) && !isActive ) {
			GetComponent<Renderer>().material.color = Color.blue;
		} else {
			GetComponent<Renderer>().material.color = Color.red;
		}
	}

	public override void Toggle(GameObject activatingObject) {
		if (oneShot) {
			if (!triggered) {
				if (FusePanelScript.CheckFuse(fuseSlotNumber)) {
					base.Toggle(activatingObject);
					triggered = true;
					sparks.SetActive(true);
				}
			}
		} else {
			if (FusePanelScript.CheckFuse(fuseSlotNumber)) {
				base.Toggle(activatingObject);
			}
		}	
	}
}
