﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: FlashlightController
/// </summary>
public class FlashlightController : Controller {
	#region Fields

	public GameObject myLight;

	#endregion

	internal override void Start() {
		base.Start();
		lr.enabled = false;
	}

	private void OnEnable() {
		//print("sdgf");
		GetComponent<LineRenderer>().enabled = false;
	
	}

	internal override void Interact() {
		base.Interact();
		//print( name + " called interact" );

		//OVRInput.Update();

		if ( OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, controller) ) {
			//print("sdfa");
			myLight.SetActive(!myLight.activeSelf);
		}
	}
}