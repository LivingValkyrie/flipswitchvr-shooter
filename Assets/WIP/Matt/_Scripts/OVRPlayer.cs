﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: OVRPlayer
/// </summary>
public class OVRPlayer : MonoBehaviour {
	#region Fields

	Vector3 vel, rot;
	public float speed = 5;

	#endregion
	
	void Start() {
		
	}

	void Update() {
		if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch)) {
			return;
		}

		vel = new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * speed,0, Input.GetAxis("Vertical" ) * Time.deltaTime * speed);

		rot = OVRInput.Get( OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch ); 
		transform.position += transform.forward * vel.z;
		transform.position += transform.right * vel.x;
		transform.Rotate(Vector3.up, rot.x, Space.Self);

	}
}