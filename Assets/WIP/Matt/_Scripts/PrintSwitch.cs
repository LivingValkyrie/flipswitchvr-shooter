﻿using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PrintSwitch
/// </summary>
public class PrintSwitch : SimpleSwitch {
	#region Fields

	public Color on = Color.white;
	public Color off = Color.black;

	#endregion
	public override void OnActivate() {
		GetComponent<Renderer>().material.color = on;
	}
	
	public override void OnDeactivate() {
		GetComponent<Renderer>().material.color = off;

	}
}