﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultitoolController : Controller {
	GameObject heldObject; //Object currently being held by multitool

	public float holdRange; //How far away from the multitool an object is being held
	public float speed = 5;
	public float holdMin; //Closest distance to the multitool an object can be held
	public float holdMax; //Furthest distance from the multitool and object can be held
	public Material holdingMat;
	public float forceSize = 10;

	internal override void Start() {
		base.Start();
		heldObject = null; //Set the currently held object to Null
	}

	internal override void Update() {
		base.Update();
		GravGun();
	}

	internal override void Interact() {
		base.Interact();
		//print( name + " called interact" );
		//OVRInput.Update();
		if ( OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, controller)) {
			//print( name + " pressed" );
			if (selected) {
				//print(selected.name + " is selected");
				if (selected.transform.GetComponentInParent<SimpleSwitch>()) {
					selected.transform.GetComponentInParent<SimpleSwitch>().Toggle(gameObject);
				}
			}
		} else if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, controller)) {
			//print( name + " released" );

			if ( heldObject) {
				heldObject.GetComponentInParent<SimpleSwitch>().Toggle(gameObject);
			}
		}


	}

	void GravGun() {
		if (heldObject == null) {
			return;
		} else {

			heldObject.transform.position = transform.position + transform.forward * holdRange; //wasnt adding pos to current pos, thats my bad

			//Player input
			if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, controller)) {
				holdRange += Input.GetAxis("Vertical") * Time.deltaTime * speed; //* by delta time so that its not near instant
			}

			//Adjust for min/max values
			if (holdRange < holdMin)
				holdRange = holdMin;
			else if (holdRange > holdMax)
				holdRange = holdMax;
		}
	}

	public void SetHeldObject(GameObject obj) {
		if (obj) {
			//todo fix null ref
			heldObject = obj;
			Vector3 distance = transform.position - heldObject.transform.position;
			holdRange = distance.magnitude;
			//print(holdRange + " " + distance);
		}
	}

	public void DropObject() {
		heldObject = null;
	}
}