﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Pressable
/// </summary>
public class Pressable : SimpleSwitch {
	#region Fields

	public UnityEvent activate, deactivate;

	#endregion
	public override void Toggle(GameObject activatingObject) {
		if (activatingObject.GetComponent<HandController>()) {
			base.Toggle(activatingObject);
		}
	}
	public override void OnActivate() {
		activate.Invoke();
	}
	public override void OnDeactivate() {
		deactivate.Invoke();
	}
}