﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Controller : MonoBehaviour {

	public ControllerSettings settings;
	public GameObject cursor;
	public bool updatePointer = true;
	[Tooltip("OVR Controller type")]
	public OVRInput.Controller controller;
	[Tooltip("Controller Type")]
	public ControllerType type;
	[Tooltip("Update via oculus api")]
	public bool updateController = true;


	#region Privates

	internal LineRenderer lr;
	Vector3[] pointer = new Vector3[2];
	internal GameObject selected;

	#endregion

	// Use this for initialization
	internal virtual void Start() {
		lr = GetComponent<LineRenderer>();
		//lr.materials = new Material[] { settings.beam };
		lr.material.color = settings.inactiveColor;
		lr.widthMultiplier = settings.lineWeight;

		if(!cursor && updatePointer){
			Debug.LogWarning (name + " has no cursur, turning off update pointer");
			updatePointer = false;
		}

	}

	// Update is called once per frame
	internal virtual void Update() {
		//OVRInput.Update();
		if (updateController) {
			UpdateController();
		}

		if (updatePointer) {
			UpdatePointer();
		} else {
			if(cursor){
				cursor.SetActive(false);
			}
			lr.enabled = false;
		}

		Interact();
	}

	internal virtual void FixedUpdate() {
		OVRInput.FixedUpdate();
	}

	//todo prolly be moved in to the child classes
	internal virtual void Interact() {
		////print(name + " called interact in base");
	}


	internal void UpdateController() {
		//transform.localPosition = OVRInput.GetLocalControllerPosition(controller);
		transform.localRotation = OVRInput.GetLocalControllerRotation(controller);
	}

	internal void UpdatePointer() {
		pointer[0] = transform.position;

		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit, settings.range)) {
			pointer[1] = hit.point;
			lr.startColor = lr.endColor = settings.activeColor;
			selected = hit.transform.gameObject;
			cursor.transform.position = hit.point;
			cursor.transform.forward = -hit.normal;
			cursor.SetActive(true);
		} else {
			pointer[1] = transform.position + transform.forward * settings.range;
			lr.startColor = lr.endColor = settings.inactiveColor;
			selected = null;
			cursor.SetActive(false);
		}

		lr.SetPositions(pointer);

		if (selected) {
			if (selected.GetComponentInParent<SimpleSwitch>()) {
				//todo change this to use smoothing from RC2D
				float perc = (Mathf.Sin(Time.time * settings.pulseRate) + 1) / 2.0f;
				lr.startColor = lr.endColor = Color.Lerp(settings.activeColor, settings.pulseColor, perc);
			}
		}

		lr.enabled = true;
	}

	void OnDrawGizmos() {
		Gizmos.DrawRay(transform.position, transform.forward * settings.range);
	}

	public enum ControllerType {
		MultiTool,
		Hand,
		Flashlight
	}
}