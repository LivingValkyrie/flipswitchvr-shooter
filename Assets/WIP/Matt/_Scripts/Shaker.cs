﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Shaker
/// </summary>
public class Shaker : MonoBehaviour {
	#region Fields

	public float speed, scale;


	#endregion
	
	void Start() {
		
	}

	void Update() {
		transform.position = new Vector3(transform.position.x + Mathf.Sin(Time.time * speed * scale),transform.position.y, transform.position.z);
	}
}