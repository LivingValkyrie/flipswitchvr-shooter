﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicScaler : MonoBehaviour {

	public float heightInInches;
	public Transform spaceSize;

	// Use this for initialization
	void Start () {
		Vector3 temp = Vector3.one * (heightInInches * 0.0254f) * 0.41f;
		print( temp + " before");
		//temp.x /= spaceSize.lossyScale.x;
		//temp.y /= spaceSize.lossyScale.y;
		//temp.z /= spaceSize.lossyScale.z;

		//print( temp + " after" );
		transform.localScale = temp;
		print( transform.lossyScale + " lossy" );
		transform.parent = spaceSize;
		print( transform.lossyScale + " lossy" );

	}


	// Update is called once per frame
	void Update () {
		
	}
}
