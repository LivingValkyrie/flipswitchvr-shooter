﻿using System.Collections;
using System.Collections.Generic;
using RootMotion.Demos;
using UnityEngine;

public class VRIKCalibrateOnStart : MonoBehaviour {

	VRIKCalibrationController mine;
	public OVRInput.Controller controller;

	// Use this for initialization
	void Start() {
		mine = GetComponent<VRIKCalibrationController>();
		StartCoroutine("Calibrate");
	}

	void Update() {
		//if (OVRInput.GetDown(OVRInput.Button.One, controller)) {
		//	mine.Calibrate();
		//}
	}

	// Update is called once per frame
	IEnumerator Calibrate() {
		yield return new WaitForEndOfFrame();
		mine.Calibrate();
	}
}